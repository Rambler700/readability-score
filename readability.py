# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 16:46:50 2019

@author: enovi
"""

import math
#Readability calculator

def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Readability Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             analyze_text(article_text)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def analyze_text(article_text):
    
    word_count = 0
    sentence_count = 0
    syllable_count = 0
    
    article_text = article_text.lower().split()
    
    for word in article_text:
        word_count += 1
        if '!' in word:
            sentence_count += 1
        if '.' in word:
            sentence_count += 1
        if '?' in word:
            sentence_count += 1
        if ';' in word:
            sentence_count += 1
        for letter in word:
            ll = letter.lower()
            if ll == 'a' or ll == 'e' or ll == 'i' or ll == 'o' or ll == 'u' or ll == 'y':
                syllable_count += 1
    
    print('Word Count Is', word_count)
    print('Sentence Count Is', sentence_count)
    print('Syllable Count Is', syllable_count)
    
    # Flesch Reading Ease
    
    fl_reading_ease = 206.835 - (1.015 * (word_count/sentence_count)) - (84.6 * (syllable_count/word_count))

    print('Flesch Reading Ease Is',fl_reading_ease)
    
    #End Reading Ease
    
    # Flesch-Kincaid Grade Level
    
    fl_grade_level = (0.39 * (word_count/sentence_count)) + (11.8 * (syllable_count/word_count)) - 15.59
    
    print('Flesch-Kincaid Grade Level Is',fl_grade_level)

    # End Grade Level

    #FORCAST
    
    single_syllable_count = 0
    for word in article_text:
        for letter in word:
            ll = letter.lower()
            if ll == 'a' or ll == 'e' or ll == 'i' or ll == 'o' or ll == 'u' or ll == 'y':
                syllable_count += 1
        if syllable_count == 1:
            single_syllable_count += 1
        syllable_count = 0
    
    single_syllable_count = single_syllable_count * (150 / word_count)    
    
    forcast_score = 20 - (single_syllable_count / 10)
    
    print('Forcast Score Is',forcast_score)
    
    # SMOG Index
    polysyllable_count = 0
    for word in article_text:
        for letter in word:
            ll = letter.lower()
            if ll == 'a' or ll == 'e' or ll == 'i' or ll == 'o' or ll == 'u' or ll == 'y':
                syllable_count += 1
        if syllable_count >= 3:
            polysyllable_count += 1
        syllable_count = 0
    
    smog_score = 1.0430 * math.sqrt(polysyllable_count * (30/sentence_count)) + 3.1291
    
    print('SMOG Score Is',smog_score)
    #End SMOG Index

main()